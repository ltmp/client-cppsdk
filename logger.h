#pragma once

class Logger {

public:

    enum Severity {
        DEBUG,
        INFO,
        WARNING,
        FATAL
    };

    virtual void log(Severity severity, std::wstring const& message) const = 0;
    virtual void log(Severity severity, std::string const& message) const = 0;

};