#pragma once

#include <vector>

class LTMP;

#include "component.h"
#include "logger.h"

#ifdef _WIN32
#ifdef __IS_LTMP
#define LTMP_API __declspec(dllimport)
#else
#define LTMP_API __declspec(dllexport)
#endif
#else
#define LTMP_API
#endif

class LTMP {

public:

    virtual void enableNametagRendering(bool enable) = 0;
    virtual bool isNametagRengeringEnabled() const = 0;
    virtual void setNametagRenderDistance(float dist) = 0;
    virtual float getNametagRenderDistance() const = 0;
    virtual void setIsNametagRenderedOnLocalPlayer(bool render) = 0;
    virtual bool isNametagRenderedOnLocalPlayer() = 0;

    virtual ComponentContainer const* getComponentContainer() const = 0;
    virtual Logger const* getLogger() const = 0;

};

//This is an entry point of module
LTMP_API void registerComponents(ComponentContainer* container);
