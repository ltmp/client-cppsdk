#pragma once

typedef uint16_t componentId_t;

class Component;

class ComponentContainer {

public:

    virtual componentId_t registerComponent(Component* component) = 0;

    /**
     * Returns nullptr if component is not registered!
     */
    virtual Component* getRawComponent(componentId_t id) const = 0;
    virtual std::vector<Component*> const& getComponents() const = 0;

    template<typename T>
    T* getComponent(componentId_t id)
    {
        return (T*) this->getRawComponent(id);
    }

};

/**
 * This should be extended by the module.
 */
class Component {

public:

    /**
     * Is triggered when component is being deleted.
     * (Accessing other components during this phase causes undefined behaviour)
     */
    virtual ~Component(){};

    /**
     * Is triggered when all components from all modules are attached.
     */
    virtual void onInitialized(LTMP const* container){};

    /**
     * Is triggered on every script tick.
     *
     * @param delta - Delta time since last tick (1.0 - 1 second)
     */
    virtual void onTick(float delta){};

    /**
     * Is triggered when component is deinitialized.
     * (Accessing other components during this phase is fine)
     */
    virtual void onDeinitialized(){};

    /**
     * Triggered when player connets to a server.
     */
    virtual void onConnected(){};

    /**
     * Triggered when all server data is synced with player.
     */
    virtual void onPrepared(){};

    /**
     * Triggered when player disconnects from a server.
     */
    virtual void onDisconnected(){};

};